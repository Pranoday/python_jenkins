import jenkins
#Connecting to a Jenkins server
server = jenkins.Jenkins('http://192.168.43.10:8080',username="Pranodayd",password="119737275fd132a08d5a3b457ed56649a2")

#Getting information about loggedin user
user = server.get_whoami()
version = server.get_version()
print('Hello %s from Jenkins %s' % (user['fullName'], version))

#Getting the configuration of existing job named "Demo"
DemoJobConfiguration=server.get_job_config("Demo")

'''
    Creating a new job named "JobCreatedUsingPythonJenkins" using configuration retrieved and saved in 
    variable DemoJobConfiguration 
'''
server.create_job('JobCreatedUsingPythonJenkins',DemoJobConfiguration)
#Printing information of all jobs,Each job information is returned in the form of Dictionary
jobs = server.get_jobs()
print(jobs)

#Triggering a build of Non-parameterized job
server.build_job('ReleaseCalculatorAPI')

#Deleting a job created
server.delete_job('JobCreatedUsingPythonJenkins')


# build a parameterized job
# Building our job "BuildAndDeployCalculatorWebApplication" with required 2 parameters
server.build_job('BuildAndDeployCalculatorWebApplication', {'CalculatorWebApplicationDeploymentDirectory': 'D:\\JenkinsBookExamples\\DeployedCalculatorWebApp\\',
                                                             'TypeOfTestsToRun': 'AllTests'})

#Retrieving build number of last build executed for job "BuildAndDeployCalculatorWebApplication"
last_build_number = server.get_job_info('BuildAndDeployCalculatorWebApplication')['lastCompletedBuild']['number']
build_info = server.get_build_info('BuildAndDeployCalculatorWebApplication', last_build_number)
print(build_info)
